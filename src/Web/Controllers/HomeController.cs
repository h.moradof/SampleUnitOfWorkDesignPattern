﻿using DatabaseContext.Context;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        #region props
        private readonly IUnitOfWork _uow;
        private readonly IBlogService _blogService;
        #endregion

        #region ctor
        public HomeController(IUnitOfWork uow, IBlogService blogService)
        {
            _uow = uow;
            _blogService = blogService;
        }
        #endregion


        public ActionResult Index(int? page = 1)
        {
            var blogs = _blogService.GetAll(page.Value, 15);

            return View(blogs);
        }

    }
}