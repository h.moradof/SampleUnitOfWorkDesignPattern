﻿using System.Web;
using System.Web.Optimization;

namespace Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // -------------------------- js  ---------------------------
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/lib/js/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Content/lib/js/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Content/lib/js/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Content/lib/js/bootstrap.js",
                      "~/Content/lib/js/respond.js"));

            // -------------------------- css  ---------------------------
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/lib/css/bootstrap.css",
                      "~/Content/site/css/me.css"));
        }
    }
}
