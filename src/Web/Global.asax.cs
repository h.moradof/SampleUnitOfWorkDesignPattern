﻿using StructureMap.Web.Pipeline;
using System;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Web.DependencyResolution;

namespace Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            // area
            AreaRegistration.RegisterAllAreas();

            // mvc and web api
           // GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // remove extra ViewEngines
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());

            // security
            MvcHandler.DisableMvcResponseHeader = true;

            // structuremap for mvc
            ControllerBuilder.Current.SetControllerFactory(new StructureMapControllerFactory());
        }



        #region Application_EndRequest

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            HttpContextLifecycle.DisposeAndClearAll();
        }

        #endregion

    }
}
