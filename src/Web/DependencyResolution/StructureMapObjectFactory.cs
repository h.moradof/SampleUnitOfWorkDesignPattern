﻿using System;
using System.Threading;
using DatabaseContext.Context;
using Services.Interfaces;
using StructureMap;  // install-package structuremap
using StructureMap.Web; // install-package structuremap.web

namespace Web.DependencyResolution
{
    /// <summary>
    /// object factory
    /// </summary>
    public static class StructureMapObjectFactory
    {
        private static readonly Lazy<Container> _containerBuilder =
            new Lazy<Container>(defaultContainer, LazyThreadSafetyMode.ExecutionAndPublication);

        public static IContainer Container
        {
            get { return _containerBuilder.Value; }
        }

        private static Container defaultContainer()
        {
            return new Container(x =>
            {
                // install-package entityframework <= for line bellow
                x.For<IUnitOfWork>().HybridHttpOrThreadLocalScoped().Use<SampleDatabaseContext>();

                x.Scan(scan =>
                {
                    scan.AssemblyContainingType<IBlogService>();
                    scan.WithDefaultConventions();
                });

                // ======================================================
                // You can choose your creation strategy like singleton
                // ======================================================

                //// user
                //x.For<ICurrentUser>().Use<CurrentUser>();

                //// cache managers
                //x.For<IApplicationSettingCacheManager>().Singleton().Use<ApplicationSettingCacheManager>();
                //x.For<ISiteSeoSettingCacheManager>().Singleton().Use<SiteSeoSettingCacheManager>();
                //x.For<IAccessCacheManager>().Singleton().Use<AccessCacheManager>();
                //x.For<IApplicationCacheManager>().Singleton().Use<ApplicationCacheManager>();

                //// mail & sms
                //x.For<IMailSender>().Singleton().Use<MailSender>();
                //x.For<ISmsSender>().Singleton().Use<SmsSender>();
                //x.For<IUserAccountMailSender>().Singleton().Use<UserAccountMailSender>();
               
                //// sms provider
                //x.For<ISmsProvider>().Singleton().Use<KaveNegarSmsProvider>().Ctor<string>("templateName").Is("Together-Vertification");

                //// api
                //x.For<ITokenManager>().Singleton().Use<TokenManager>();

                //// webservice
                //x.For<IWebServiceManager>().Singleton().Use<WebServiceManager>();

            });
        }

    }
}