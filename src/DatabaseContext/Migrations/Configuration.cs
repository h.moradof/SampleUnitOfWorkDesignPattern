namespace DatabaseContext.Migrations
{
    using DomainModels.Entities.Site;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DatabaseContext.Context.SampleDatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(DatabaseContext.Context.SampleDatabaseContext context)
        {
            if (!context.Set<Blog>().Any())
            {
                for (int i = 0; i < 30; i++)
                {
                    context.Set<Blog>().Add(new Blog { Name = "sample blog " + i });
                }
                context.SaveChanges();
            }
        }
    }
}
