﻿using DomainModels.Entities.Base;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading;
using System.Threading.Tasks;

namespace DatabaseContext.Context
{
    public interface IUnitOfWork : IDisposable
    {
        IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity;

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        int SaveChanges();

        DbEntityEntry<TEntity> Entry<TEntity>(TEntity Entity) where TEntity : class;

        Database Database { get; }
    }
}
