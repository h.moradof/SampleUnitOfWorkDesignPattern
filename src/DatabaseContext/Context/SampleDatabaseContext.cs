﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Reflection;
using System.Linq;
using DomainModels.Entities.Base;
using System.Data.Entity.Validation;
using DatabaseContext.ErrorFormater;
using System.Threading.Tasks;
using System.Threading;
using DatabaseContext.Migrations;

namespace DatabaseContext.Context
{
    public class SampleDatabaseContext : DbContext, IUnitOfWork
    {

        #region ctor

        public SampleDatabaseContext(string connectionStringName)
            : base(connectionStringName)
        { }

        public SampleDatabaseContext()
            : base("DefaultConnectionString")
        {   }

        #endregion


        #region static ctor

        static SampleDatabaseContext()
        {
            // On server
            // Database.SetInitializer<SampleDatabaseContext>(null);

            // // On local host
            Database.SetInitializer<SampleDatabaseContext>(new MigrateDatabaseToLatestVersion<SampleDatabaseContext, Configuration>());

            // On Test
            //  Database.SetInitializer<SampleDatabaseContext>(new DropCreateDatabaseAlways<SampleDatabaseContext>());
        }

        #endregion


        #region OnModelCreating

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // remove PluralizingTableNameConvention
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            // decimal precision
            CorrectiveDecimalPrecisions(modelBuilder);

            // delete strategies
            SetDeleteStrategies(modelBuilder);

            // load props
            var asm = Assembly.GetAssembly(typeof(DomainModels.Entities.Base.BaseEntity));
            LoadEntities(asm, modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        #endregion


        #region Set Delete Strategies

        private static void SetDeleteStrategies(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        #endregion


        #region Corrective decimal properties

        /// <summary>
        /// Corrective decimal precision
        /// </summary>
        /// <param name="modelBuilder"></param>
        private static void CorrectiveDecimalPrecisions(DbModelBuilder modelBuilder)
        {
            
        }

        #endregion


        #region LoadEntities

        /// <summary>
        /// Load Entities into db context
        /// </summary>
        /// <param name="asm"></param>
        /// <param name="modelBuilder"></param>
        private void LoadEntities(Assembly asm, DbModelBuilder modelBuilder)
        {
            var entityTypes = asm.GetTypes()
                                    .Where(type => type.BaseType != null &&
                                           type.BaseType.IsAbstract &&
                                           !type.IsAbstract &&
                                           (type.BaseType == typeof(BaseEntity) || type.BaseType.BaseType == typeof(BaseEntity))
                                           )
                                    .ToList();

            var entityMethod = typeof(DbModelBuilder).GetMethod("Entity");
            entityTypes.ForEach(type =>
            {
                entityMethod.MakeGenericMethod(type).Invoke(modelBuilder, new object[] { });
            });
        }

        #endregion


        #region IUnitOfWork Members

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                throw new FormattedDbEntityValidationException(e);
            }
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            try
            {
                return base.SaveChangesAsync(cancellationToken);
            }
            catch (DbEntityValidationException e)
            {
                throw new FormattedDbEntityValidationException(e);
            }
        }

        #endregion

    }
}