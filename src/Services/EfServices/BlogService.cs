﻿using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using DomainModels.Entities.Site;
using ViewModels.Site;
using System.Data.Entity;
using DatabaseContext.Context;

namespace Services.EfServices
{
    public class BlogService : IBlogService
    {
        #region props
        private readonly IDbSet<Blog> _blogs;
        #endregion

        #region ctor
        public BlogService(IUnitOfWork uow)
        {
            _blogs = uow.Set<Blog>();
        }
        #endregion



        public Blog Add(Blog model)
        {
            return _blogs.Add(model);
        }

        public Blog Edit(Blog model)
        {
            throw new NotImplementedException();
        }

        public Blog Find(long id)
        {
            return _blogs.FirstOrDefault(c => c.Id == id);
        }

        public List<BlogShowListViewModel> GetAll(int pageNumber, int count)
        {
            return _blogs.OrderByDescending(c => c.Id)
                .Select(c => new BlogShowListViewModel
                {
                    Id = c.Id,
                    Name = c.Name,
                    CreatedOn = c.CreatedOn
                })
                .Skip((pageNumber - 1) * count)
                .Take(count)
                .ToList();
        }
    }
}
