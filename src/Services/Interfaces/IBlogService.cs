﻿using DomainModels.Entities.Site;
using System.Collections.Generic;
using ViewModels.Site;

namespace Services.Interfaces
{
    public interface IBlogService
    {
        Blog Add(Blog model);
        Blog Edit(Blog model);
        Blog Find(long id);
        List<BlogShowListViewModel> GetAll(int pageNumber, int count);
    }
}
