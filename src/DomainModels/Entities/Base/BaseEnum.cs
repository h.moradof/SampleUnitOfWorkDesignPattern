﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.Base
{
    public abstract class BaseEnum : BaseEntity
    {
        [Column(Order = 1)]
        [DisplayName("عنوان")]
        [Required(ErrorMessage = "{0} الزامی است")]
        [StringLength(128, ErrorMessage = "حداکثر طول مجاز برای {0} {1} کاراکتر می باشد")]
        public virtual string Name { get; set; }
    }
}
