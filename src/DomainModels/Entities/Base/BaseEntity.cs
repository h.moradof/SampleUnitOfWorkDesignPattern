﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModels.Entities.Base
{
    public abstract class BaseEntity : ICloneable
    {
        [Key]
        [Column(Order = 0)]
        [DisplayName("شناسه")]
        public virtual long Id { get; set; }
        
        // other props

        [Column(Order = 101)]
        [DisplayName("تاریخ ثبت")]
        public virtual DateTime CreatedOn { get; set; }

        [Column(Order = 102)]
        [DisplayName("تاریخ آخرین ویرایش")]
        public virtual DateTime? UpdatedOn { get; set; }

        [Column(Order = 103)]
        [DisplayName("تاریخ حذف")]
        [ScaffoldColumn(false)]
        public virtual DateTime? DeletedOn { get; set; }

        public BaseEntity()
        {
            CreatedOn = DateTime.Now;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
