﻿using System;

namespace ViewModels.Site
{
    public class BlogShowListViewModel
    {
        // This class is just for test. in real projects we use ViewModel classes when we need some extra properties
        // or need some properties from two or more Entities
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
